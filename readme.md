Simple text editor

Features
-------------------

- Speech to text (Windows only)

- Supported File formats: html or plain text

- Export to: docx

Start App
------------

### Uninstalled

`python3.7 -m textedit`

### Installed

`textedit`

Dependencies
--------------

### Linux

- portaudio19-dev

- python3-all-dev

### Python3

- mammoth

- PyAudio

- SpeechRecognition

