Add new icons
==============

PyQt4 or PyQt5 normally uses a resource file, rather than loading individual 
image files.

How to make a resource file
------------------------------

- Copy icon files into `icons` folder

- Update icons.qrc

- From the package base folder run this command

    pyrcc5 icons.qrc -o gui/icons_rc.py

For PyQt4 use `pyrcc4` rather than `pyrcc5`

Neither `pyrcc4` nor `pyrcc5` need to be packaged with the app. They are only 
for making a resource file and nothing more.

Ensure there is a `gui` folder before running this command

The `gui` folder should contain a `__init__.py` file
This file should contain this import command

    from . import icons_rc