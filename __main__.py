#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
''' This is from where the PyQt4/PyQt5 application is launched.
    To launch the app,
    
    `python3.7 -m pyqt4-textedit`
    
    You need to be in the project folder (parent of the package folder) for 
    this command to work
'''

import sys

from .gui.textedit import main

if __name__ == '__main__':
    #Put app launching code here
    main()