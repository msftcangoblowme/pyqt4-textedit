# Importing Modules

import sys 
import os
import subprocess
from pathlib import Path

if sys.version_info[:2] >= (3, 7):
    from PyQt5.QtCore import (Qt, QSize, pyqtSlot, pyqtSignal)

    from PyQt5.QtGui import (QIcon, QFont, QTextListFormat, QImage, \
QTextOption, QCloseEvent)

    from PyQt5.QtWidgets import (QApplication, QMainWindow, QMessageBox, QAction, \
QFontComboBox, QComboBox, QTabWidget, QTextEdit, QFileDialog, \
QDialog, QFontDialog, QColorDialog, QLabel, QPushButton)

    from PyQt5.QtPrintSupport import (QPrinter, QPrintDialog, \
QPrintPreviewDialog)
else:
    from PyQt4.QtGui import *
    from PyQt4.QtCore import *

import time
try:
    import mammoth
except ImportError as e:
    mammoth = None

try:
    import speech_recognition as sr
except ImportError as e:
    sr = None

__all__ = ['main']

class Tab(QTabWidget):
    def __init__(self, parent=None, title="New tab"):
        super().__init__(parent=parent)
        
        self._title = title if isinstance(title, str) and len(title)!=0 else "New tab"
        
        #TabWidget code
        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self._save_tab_by_index)
        self.setTabShape(QTabWidget.Triangular)
        self.setStyleSheet("QTextEdit{background-color: #ffffff}")
        self._text_font = QFont("Consolas",15,) 
        
        #Tab code
        self.add_tab()
    
    def new_file(self):
        self.add_tab()
        self.setCurrentIndex(self.count()-1)
    
    def _new_textedit(self):
        textEdit = QTextEdit()
        textEdit.setUndoRedoEnabled(True)
        textEdit.setAcceptRichText(False)
        textEdit.setStyleSheet("QTextEdit{font-size:20px;color: #000000}")
        textEdit.setWordWrapMode(QTextOption.NoWrap) 
        textEdit.setTabStopWidth(12)
        textEdit.setFont(self._text_font)
        return textEdit
    
    def add_tab(self):
        ''' The QTextEdit is accessible from, `self.widget(index)`. 
            `self.textEdit` is obsolete.
        '''
        textedit = self._new_textedit()
        if isinstance(textedit, QTextEdit):
            self.addTab(textedit, self._title)
    
    @property
    def html(self):
        widget = self.currentWidget()
        return widget.toHtml()
    
    @property
    def plain_text(self):
        widget = self.currentWidget()
        return widget.toPlainText()
    
    @property
    def text(self):
        widget = self.currentWidget()
        return widget.text()
    
    @text.setter
    def text(self, the_text):
        widget = self.currentWidget()
        if isinstance(the_text, str) and len(the_text)!=0:
            widget.setText(the_text)
    
    @pyqtSlot(int)
    def remove_tab(self, index):
        widget = self.widget(index)
        if widget: widget.deleteLater()
        self.removeTab(index)
    
    def remove_tab_current(self):
        index = self.currentIndex()
        self.remove_tab(index)
    
    @pyqtSlot(int)
    def _save_tab_by_index(self, index):
        ''' Asks before whether to save or not. If yes, calls, 
            `class`:`_save_tab`
        '''
        choice = QMessageBox.question(self,'You have changes!',f'Save tab {self._tab.count()}?', \
                       QMessageBox.Save | QMessageBox.Discard|QMessageBox.Cancel)
        if choice == QMessageBox.Save:
            self._save_tab(index)
        elif choice == QMessageBox.Discard:
            #Remove tab
            self.remove_tab_current()
    
    def _save_tab(self, index, is_remove=True, title="Save file"):
        widget = self.widget(index)
        html = self.html
        plain_text = self.plain_text
        changes_saved = False
        
        ''' There is no list[dict] with the tab index & file name.
            There is no mechanism to save the tab file name and not 
            save the tab.
            #removed...
            #self.filename = 
        '''
        filename = QFileDialog.getSaveFileName(self, title, "", \
               "All Files(*.*);;txt(*.txt);;;pdf(*.pdf);;writer(*.writer)")
        
        file_path = filename[0] if isinstance(filename, tuple) else None
        if file_path:
            path_file = Path(file_path)
            path_file.resolve()
            #remove '.'
            extension = path_file.suffix[1:]
        
            if file_path.endswith(".writer"):       
                path_file.write_text(html)
                changes_saved = True
            else:
                ''' icons for the files.
                    We just store the contents of the text file along with the
                    format in html, which Qt does in a very nice way for us
                    Checking whether there is an Extension or not
                '''
                try:
                    # All other extensions, save as plain text
                    path_file.write_text(plain_text)
                    '''
                    with open(filename,"wt") as f:
                        f.write(plain_text)
                    '''
                    changes_saved = True
                except Exception as e:
                    print(f"Saving to file failed. {str(e)}")
            if changes_saved and is_remove:
                self.remove_tab(index)
                self.setCurrentIndex(self.count()-1)
    
    def save_file(self, index=None):
        ''' Saves current tab to file
            Not appropriate for saving all open tabs while exiting app.
        '''
        int_index = self.currentIndex() if index is None else index
        
        self._save_tab(int_index, is_remove=True, title="Save file")
    
    def save_file_as(self, index=None):
        ''' Saves current tab to file
            Not appropriate for saving all open tabs while exiting app
        '''
        int_index = self.currentIndex() if index is None else index
        self._save_tab(int_index, is_remove=True, title="Save as")
    
    def open_file(self, title="Open File"):
        # Change textEdit to QWebview
        # Read contents of the pdf file
        # SetContent() of the pdf to the QWebView

        filename = QFileDialog.getOpenFileName(self,title, os.getenv('HOME'), \
              "All files(*.*);;(*.pdf);;txt(*.txt);;;writer(*.writer)")
        file_path = filename[0] if isinstance(filename, tuple) else None
        if file_path:
            path_file = Path(file_path)
            path_file.resolve()
            #remove '.'
            extension = path_file.suffix[1:]
            if extension=="pdf":
                ''' This below line opens our pdfViewer.exe file which is 
                    selected from PopUp file picker
                    
                    Use QProcess, not subprocess. QProcess has event 
                    support. Just need to make a sublcass.
                    
                    Put isn't this opening up a separate app?! Maybe 
                    subprocess isn't appropriate
                '''
                subprocess.check_call([r"pdf complete\pdfvista.exe", file_path])
            else:
                with path_file.open(mode='r') as f:
                    if self.count()==0:
                        self.new_file()
                    file_text = f.read()
                    if isinstance(file_text, str) and len(file_text)!=0:
                        self._tab.text = file_text
                    #Save file path for this tab
                    #self.current_save_file_path = name
                    pass
    
    def insert_image(self, title="Insert image"):
        '''insert image into textEdit box'''
        widget = self.currentWidget()

        # Get image file name
        filename = QFileDialog.getOpenFileName(self, title,".","Images (*.png *.xpm *.jpg *.bmp *.gif)")
        
        file_path = filename[0] if isinstance(filename, tuple) else None
        if file_path:
            path_file = Path(file_path)
            path_file.resolve()
            #remove '.'
            extension = path_file.suffix[1:]
        
            # Create image object
            image = QImage(file_path)
            
            # Error if unloadable
            if image.isNull():
                popup = QMessageBox(QMessageBox.Critical, \
                                          "Image load error", \
                                          "Could not load image file!", \
                                          QMessageBox.Ok, self.parent)
                popup.show()
            else:
                cursor = widget.textCursor()
                cursor.insert_image(image, file_path)
    
    def mic(self):
        #voice recognition
        widget = self.currentWidget()
        if sr:
            r = sr.Recognizer()
            with sr.Microphone() as source:
                r.adjust_for_ambient_noise(source)
                print("Speak Anything :")
                audio = r.listen(source)
                try:
                    print("got It.. now recognising...")
                    text = r.recognize_google(audio, language="en-US")
                    print(f"You said : {text}")
                    widget.append(text)
                except Exception as e:
                    print("Sorry. Could not recognize what you said")
                    widget.append("Sorry. Could not recognize what you said")
    
    def print_doc(self):
        widget = self.currentWidget()
        dialog = QPrintDialog()
        if dialog.exec_() == QDialog.Accepted:
            widget.document().print(dialog.printer())
    
    def date_insert(self):
        widget = self.currentWidget()
        #print(time.strftime("%d/%m/%Y %I:%M:%S"))
        widget.insertHtml(time.strftime("%d/%m/%Y"))
    
    def time_insert(self):
        widget = self.currentWidget()
        #print(time.strftime("%d/%m/%Y %I:%M:%S"))
        widget.insertHtml(time.strftime("%I:%M:%p"))
    
    def cut(self):
        widget = self.currentWidget()
        widget.cut()
    
    def copy(self):
        widget = self.currentWidget()
        widget.copy()
    
    def paste(self):
        widget = self.currentWidget()
        widget.paste()
    
    def undo(self):
        widget = self.currentWidget()
        widget.undo()
    
    def redo(self):
        widget = self.currentWidget()
        widget.redo()
    
    def paint_page_view(self, printer):
        widget = self.currentWidget()
        widget.print_(printer) 
    
    def save_to_pdf(self, title="Save as PDF"):
        widget = self.currentWidget()
        
        filename = QFileDialog.getSaveFileName(self,title,'Untitled','documents(*.PDF)')
        file_path = filename[0] if isinstance(filename, tuple) else None
        if file_path:
            path_file = Path(file_path)
            path_file.resolve()
            if path_file.exists():
                printer = QPrinter(QPrinter.HighResolution)
                printer.setPageSize(QPrinter.A4)
                printer.setColorMode(QPrinter.Color)
                printer.setOutputFormat(QPrinter.PdfFormat)
                printer.setOutputFileName(name)
                widget.document().print_(printer)
    
    def is_word_wrap(self):
        widget = self.currentWidget()
        wrap_mode = widget.lineWrapMode()
        return False if QTextEdit.NoWrap else True
    
    def toggle_word_wrap(self):
        widget = self.currentWidget()
        if self.is_word_wrap():
            widget.setWordWrapMode(QTextOption.NoWrap)
        else:
            widget.setWordWrapMode(QTextOption.WrapAtWordBoundaryOrAnywhere)
    
    def font_dialog(self):
        widget = self.currentWidget()
        font, ok = QFontDialog.getFont()
        if ok:
            widget.setFont(font)
        else:
            font = QFont("Consolas")
            widget.setCurrentFont(font)
    
    def color_dialog(self):
        widget = self.currentWidget()
        color = QColorDialog.getColor()
        widget.setTextColor(color)
    
    def number_list_insert(self):
        widget = self.currentWidget()
        cursor = widget.textCursor()
        
        # Insert list with numbers
        cursor.insertList(QTextListFormat.ListDecimal)
    
    def bold(self):
        widget = self.currentWidget()
        if widget.fontWeight() == QFont.Bold:
            widget.setFontWeight(QFont.Normal)
        else:
            widget.setFontWeight(QFont.Bold)
    
    def italic(self):
        widget = self.currentWidget()
        state = widget.fontItalic()
        widget.setFontItalic(not state)
    
    def underline(self):
        widget = self.currentWidget()
        state = widget.fontUnderline()
        widget.setFontUnderline(not state)
    
    def align_left(self):
        widget = self.currentWidget()
        widget.setAlignment(Qt.AlignLeft)
    
    def align_center(self):
        widget = self.currentWidget()
        widget.setAlignment(Qt.AlignCenter)
    
    def align_right(self):
        widget = self.currentWidget()
        widget.setAlignment(Qt.AlignRight)
    
    def align_justify(self):
        widget = self.currentWidget()
        widget.setAlignment(Qt.AlignJustify)
    
    def bullet_list(self):
        widget = self.currentWidget()
        cursor = widget.textCursor()
        text = cursor.selectedText() 
        cursor.insertList(QTextListFormat.ListDecimal)  
        cursor.insertText(text)
    
    def font_family(self, font):
        widget = self.currentWidget()
        if isinstance(font, QFont):
            if widget.fontFamily()!=font.toString():
                widget.setCurrentFont(font)
        
    def font_size(self, fsize):
        widget = self.currentWidget()
        if isinstance(fsize, int):
            widget.setFontPointSize(fsize)
    
    pass
    
class Window(QMainWindow):
    
    def __init__(self):
        super().__init__()
        
        #Tab manager
        self._tab = Tab(parent=self)
        
        # Defining Properties of Editor
        self.setWindowIcon(QIcon(":/icons/text_editor.png"))
        self.setWindowTitle('TypeIt Mini Text Editor')
        self.statusbar= self.statusBar()
        self.statusbar.setStyleSheet("QStatusBar{background:#C71585;color:white}")
        self.statusbar.showMessage("Qt Mini text Editor Application Launched!!")
        
        self.setGeometry(50, 50, 640, 300)
        self.filename = ""
        self.wrap_mode = False
        self.saved_data = ""
        self.changesSaved = True

        self.menu()
        self.setCentralWidget(self._tab)
        self.show()

    def menu(self):
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('File')
        edit = mainMenu.addMenu('Edit')
        insertMenu = mainMenu.addMenu('Insert')
        view = mainMenu.addMenu('View')
        formatMenu = mainMenu.addMenu('Format')
        helpMenu = mainMenu.addMenu('Help')
        
        newFileAction = QAction(QIcon(":/icons/new.png"),'New', self)
        newFileAction.setShortcut('Ctrl+N')
        newFileAction.setStatusTip('Open a File')
        newFileAction.triggered.connect(self.new_file)
        
        openAction = QAction(QIcon(":/icons/open.png"),"Open ",self)
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open a File')
        openAction.triggered.connect(self.open_file)
        
        saveFileAction = QAction(QIcon(":/icons/save.png"),"Save",self)
        saveFileAction.setShortcut("Ctrl+S")
        saveFileAction.setStatusTip("Save document")
        saveFileAction.triggered.connect(self.save_file)
        
        #Hyphens not allowed when using resource file
        saveAsFileAction = QAction(QIcon(":/icons/save_as.png"),"Save As...",self)
        saveAsFileAction.setShortcut("Ctrl+Shift+S")
        saveAsFileAction.setStatusTip("Save document")
        saveAsFileAction.triggered.connect(self.save_file_as)
        
        #image insert
        imageAction = QAction(QIcon(":/icons/image.png"),"Insert image",self)
        imageAction.setShortcut("Ctrl+Shift+I")
        imageAction.setStatusTip("Insert image")
        imageAction.triggered.connect(self.insert_image)
        
        #insert from voice
        voiceAction = QAction(QIcon(":/icons/mike.png"),"Speech2Text",self)
        voiceAction.setShortcut("Ctrl+Shift+v")
        voiceAction.setStatusTip("Insert from voice")
        voiceAction.triggered.connect(self.mic)
        
        exitAction = QAction(QIcon(':/icons/exit.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        voiceAction.setStatusTip("Exit app. Gracefully save open tabs")
        exitAction.triggered.connect(self.exit_function)
        
        printAction = QAction(QIcon(":/icons/print.png"),"Print document",self)
        printAction.setShortcut("Ctrl+P")
        printAction.setStatusTip("Print document")
        printAction.triggered.connect(self.print_doc)
        
        dateAction = QAction(QIcon(":/icons/calender.png"),"Date",self)
        dateAction.setShortcut("Ctrl+d")
        dateAction.setStatusTip("Insert date")
        dateAction.triggered.connect(self.date_insert)
        
        timeAction = QAction(QIcon(":/icons/date_and_time.png"),"Time",self)
        timeAction.setShortcut("Ctrl+t")
        timeAction.setStatusTip("Insert time")
        timeAction.triggered.connect(self.time_insert)
        
        cutAction = QAction(QIcon(":/icons/cut.png"),"Cut to clipboard",self)
        cutAction.setShortcut("Ctrl+X")
        cutAction.setStatusTip("Delete and copy text to clipboard")
        cutAction.triggered.connect(self.cut)
        
        copyAction = QAction(QIcon(":/icons/copy.png"),"Copy to clipboard",self)
        copyAction.setShortcut("Ctrl+C")
        copyAction.setStatusTip("Copy text to clipboard")
        copyAction.triggered.connect(self.copy)
        
        pasteAction = QAction(QIcon(":/icons/paste.png"),"Paste from clipboard",self)
        pasteAction.setShortcut("Ctrl+V")
        pasteAction.setStatusTip("Paste text from clipboard")
        pasteAction.triggered.connect(self.paste)
        
        undoAction = QAction(QIcon(":/icons/undo.png"),"Undo last action",self)
        undoAction.setShortcut("Ctrl+Z")
        undoAction.setStatusTip("Undo last action")
        undoAction.triggered.connect(self.undo)
        
        redoAction = QAction(QIcon(":/icons/redo.png"),"Redo last undone thing",self)
        redoAction.setShortcut("Ctrl+Y")
        redoAction.setStatusTip("Redo last undone thing")
        redoAction.triggered.connect(self.redo)
        
        previewAction = QAction(QIcon(":/icons/if_preview.png"),"Page view",self)
        previewAction.setShortcut("Ctrl+Shift+P")
        previewAction.setStatusTip("Preview page before printing")
        previewAction.triggered.connect(self.page_view)
        
        pdfAction = QAction(QIcon(":/icons/acp_pdf.png"),"Save To PDF File",self)
        pdfAction.setStatusTip("Save PDF to Document")
        pdfAction.triggered.connect(self.save_to_pdf)
        
        self.wrapAction = QAction("WordWrap",self)  
        self.wrapAction.triggered.connect(self.word_wrap)

        fontAction = QAction(QIcon(":/icons/font.png"),'Font', self)
        fontAction.triggered.connect(self.font_dialog)
        fontAction.setStatusTip('SetTextTont')

        colorAction = QAction(QIcon(":/icons/color.png"),'Color', self)
        colorAction.triggered.connect(self.color_dialog)
        colorAction.setStatusTip('SetTextColor')
        
        numberedAction = QAction(QIcon(":/icons/number.png"),"Insert numbered List",self)
        numberedAction.setShortcut("Ctrl+Shift+L")
        numberedAction.setStatusTip("Insert numbered list")
        numberedAction.triggered.connect(self.number_list_insert)

        boldAction = QAction(QIcon(":/icons/bold.png"),"Bold",self)
        boldAction.triggered.connect(self.bold)

        italicAction = QAction(QIcon(":/icons/italic.png"),"Italic",self)
        italicAction.triggered.connect(self.italic)
        
        underlAction = QAction(QIcon(":/icons/underline.png"),"Underline",self)
        underlAction.triggered.connect(self.underline)
        
        align_left = QAction(QIcon(":/icons/align_left.png"),"Align left",self)
        align_left.triggered.connect(self.align_left)
 
        align_center = QAction(QIcon(":/icons/align_center.png"),"Align center",self)
        align_center.triggered.connect(self.align_center)
 
        align_right = QAction(QIcon(":/icons/align_right.png"),"Align right",self)
        align_right.triggered.connect(self.align_right)
 
        align_justify = QAction(QIcon(":/icons/align-justify.png"),"Align justify",self)
        align_justify.triggered.connect(self.align_justify)

        bulletAction = QAction(QIcon(":/icons/bullet.png"),"Insert Bullet List",self)
        bulletAction.triggered.connect(self.bullet_list) 

        helpAction = QAction(QIcon(":/icons/about.png"),"More Info",self)
        helpAction.triggered.connect(self.help_menu) 

        # added by Patrick
        self.toolbar2Height = 20
        self.fontFamily = QFontComboBox(self)
        # added by Patrick
        self.fontFamily.setMinimumHeight(self.toolbar2Height)
        self.fontFamily.currentFontChanged.connect(self.font_family)
 
        self.fontSize = QComboBox(self)
        self.fontSize.setMinimumHeight(self.toolbar2Height)
        self.fontSize.setEditable(True)
        self.fontSize.setMinimumContentsLength(3)
        self.fontSize.activated.connect(self.font_size)
        flist = ['6','7','8','9','10','11','12','13','14','15','16','18', \
        '20','22','24','26','28','32','36','40','44','48','54','60','66', \
        '72','80','88','96']
        self.fontSize.addItems(flist)
        
        self.toolbar = self.addToolBar("Options")
        # self.toolbar.setMaximumWidth(500)
        self.toolbar.setFloatable(False)
        self.toolbar.setMovable(False)
        self.toolbar.addSeparator()
        self.toolbar.setIconSize(QSize(16,16))
        self.toolbar.addAction(newFileAction)
        self.toolbar.addAction(openAction)
        self.toolbar.addAction(saveFileAction)
        self.toolbar.addAction(saveAsFileAction)
       
        self.toolbar.addAction(pdfAction)
        self.toolbar.addAction(printAction)
        self.toolbar.addSeparator()
        self.toolbar.addAction(undoAction)
        self.toolbar.addAction(redoAction)
        self.toolbar.addAction(cutAction)
        self.toolbar.addAction(copyAction)
        self.toolbar.addAction(pasteAction)
        
        self.toolbar.addAction(boldAction)
        self.toolbar.addAction(italicAction)
        self.toolbar.addAction(underlAction)
        self.toolbar.addAction(align_left)
        self.toolbar.addAction(align_center)
        self.toolbar.addAction(align_right)
        self.toolbar.addAction(align_justify)
        self.toolbar.addAction(bulletAction)
        self.toolbar.addSeparator()
        
        self.addToolBarBreak()
        # by Patrick. I broke up the toolbar and created a second tool bar
        self.toolbar2 = self.addToolBar('FontToolbar')
        self.toolbar2.setFloatable(False)
        self.toolbar2.setMovable(False)
        self.toolbar2.setMinimumHeight(self.toolbar2Height)
        self.toolbar2.addSeparator()
        self.toolbar2.addWidget(self.fontFamily)
        self.toolbar2.addWidget(self.fontSize)
        self.toolbar2.addSeparator()
        self.toolbar2.addAction(imageAction)
        
        #voice action in toolbar
        self.toolbar2.addAction(voiceAction)
        
        self.toolbar2.addSeparator()
        
        fileMenu.addAction(newFileAction)
        fileMenu.addAction(openAction)
        fileMenu.addAction(saveFileAction)
        fileMenu.addAction(saveAsFileAction)
        fileMenu.addAction(printAction)
        fileMenu.addAction(previewAction)
        fileMenu.addAction(pdfAction)
        fileMenu.addAction(exitAction)
        
        insertMenu.addAction(dateAction)
        insertMenu.addAction(timeAction)
        insertMenu.addAction(imageAction)
        
        #voice recognizer action
        insertMenu.addAction(voiceAction)
        
        edit.addSeparator()
        edit.addAction(undoAction)
        edit.addAction(redoAction)
        edit.addAction(cutAction)
        edit.addAction(copyAction)
        edit.addAction(pasteAction)
        
        view.addAction(self.wrapAction)
        view.addAction(numberedAction)
        
        formatMenu.addAction(fontAction)
        formatMenu.addAction(colorAction)
        formatMenu.addAction(boldAction)
        formatMenu.addAction(italicAction)
        formatMenu.addAction(underlAction)
        formatMenu.addAction(align_left)
        formatMenu.addAction(align_center)
        formatMenu.addAction(align_right)
        formatMenu.addAction(align_justify)
        formatMenu.addAction(bulletAction)
        formatMenu.addAction(fontAction)
        formatMenu.addAction(colorAction)
        
        helpMenu.addAction(helpAction)
    
    @pyqtSlot(bool)
    def open_file(self, if_checkable_is_checked=None):
        self._tab.open_file()
    
    @pyqtSlot(bool)
    def new_file(self, if_checkable_is_checked=None):
        self._tab.new_file()
    
    @pyqtSlot(bool)
    def save_file(self, if_checkable_is_checked=None):
        self._tab.save_file()
    
    @pyqtSlot(bool)
    def save_file_as(self, if_checkable_is_checked=None):
        self._tab.save_file_as()
    
    @pyqtSlot(bool)
    def insert_image(self, if_checkable_is_checked=None):
        self._tab.insert_image()
    
    @pyqtSlot(bool)
    def mic(self, if_checkable_is_checked=None):
        self._tab.mic()
    
    @pyqtSlot(bool)
    def exit_function(self, if_checkable_is_checked=None):
        ''' There may be open tabs. For each tab, the save status is 
            unknown. Save all remaining open tabs. One at a time. '''
        is_cancelled = False
        while self._tab.count()!=0 and not is_cancelled:
            self._tab.setCurrentIndex(self._tab.count()-1)
            choice = QMessageBox.question(self,'You have changes!',f'Save tab {self._tab.count()}?', \
                       QMessageBox.Yes | QMessageBox.No)
            if choice == QMessageBox.Yes:
                self.save_file()
            else:
                is_cancelled = True
        
        if not is_cancelled: sys.exit(0)
    
    @pyqtSlot(bool)
    def print_doc(self, if_checkable_is_checked=None):
        self._tab.print_doc()
    
    @pyqtSlot(bool)
    def date_insert(self, if_checkable_is_checked=None):
        self._tab.date_insert()
    
    @pyqtSlot(bool)
    def time_insert(self, if_checkable_is_checked=None):
        self._tab.time_insert()
    
    @pyqtSlot(bool)
    def cut(self, if_checkable_is_checked=None):
        self._tab.cut()
    
    @pyqtSlot(bool)
    def copy(self, if_checkable_is_checked=None):
        self._tab.copy()
 
    @pyqtSlot(bool)
    def paste(self, if_checkable_is_checked=None):
        self._tab.paste()
    
    @pyqtSlot(bool)
    def undo(self, if_checkable_is_checked=None):
        self._tab.undo()
    
    @pyqtSlot(bool)
    def redo(self, if_checkable_is_checked=None):
        self._tab.redo()
    
    @pyqtSlot(bool)
    def page_view(self, if_checkable_is_checked=None):
        preview = QPrintPreviewDialog()
        preview.paintRequested.connect(self._tab.paint_page_view)
        preview.exec_()
    
    @pyqtSlot(bool)
    def save_to_pdf(self, if_checkable_is_checked=None):
        self._tab.save_to_pdf()
    
    @pyqtSlot(bool)
    def word_wrap(self, if_checkable_is_checked=None):
        if self._tab.is_word_wrap():
            #wrap mode on
            self.statusBar().showMessage("wrap mode off")
            self.wrapAction.setIcon(QIcon(':/icons/False.png'))
        else:
            self.statusBar().showMessage("wrap mode on")
            self.wrapAction.setIcon(QIcon(':/icons/True.png'))
        self._tab.toggle_word_wrap()
    
    @pyqtSlot(bool)
    def font_dialog(self, if_checkable_is_checked=None):
        self._tab.font_dialog()
    
    @pyqtSlot(bool)
    def color_dialog(self, if_checkable_is_checked=None):
        self._tab.color_dialog()
    
    @pyqtSlot(bool)
    def number_list_insert(self, if_checkable_is_checked=None):
        self._tab.number_list_insert()
    
    @pyqtSlot(bool)
    def bold(self, if_checkable_is_checked=None):
        self._tab.bold()
    
    @pyqtSlot(bool)
    def italic(self, if_checkable_is_checked=None):
        self._tab.italic()
    
    @pyqtSlot(bool)
    def underline(self, if_checkable_is_checked=None):
        self._tab.underline()
    
    @pyqtSlot(bool)
    def align_left(self, if_checkable_is_checked=None):
        self._tab.align_left()
    
    @pyqtSlot(bool)
    def align_center(self, if_checkable_is_checked=None):
        self._tab.align_center()
    
    @pyqtSlot(bool)
    def align_right(self, if_checkable_is_checked=None):
        self._tab.align_right()
    
    @pyqtSlot(bool)
    def align_justify(self, if_checkable_is_checked=None):
        self._tab.align_justify()
    
    @pyqtSlot(bool)
    def bullet_list(self, if_checkable_is_checked=None):
        self._tab.bullet_list()
    
    @pyqtSlot(QFont)
    def font_family(self, font):
        self._tab.font_family(font)
    
    @pyqtSlot(int)
    def font_size(self, fsize):
        self._tab._font_size(fsize)
    
    def help_menu(self):
        self.helpwindow = QDialog()
        self.helpwindow.setWindowTitle('Type it....')
        self.helpwindow.setWindowIcon(QIcon(":/icons/text_editor.png"))
        #self.helpwindow.setFixedSize(400,200)
        self.helpwindow.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        #========================== edited help menu
        
        err_msg = [	'This is simple text editor app, made by Ryan Antony', \
			'Along with CID, an education hub, and team.', \
			'We hope you enjoy the app.', \
			'For any help visit:', \
			'fb.me/cideduhub', \
			'https://www.youtube.com/c/cidaneducationhub']
        msg = '\n'.join(err_msg)
        del err_msg
        
        Lbl = QLabel(msg,self.helpwindow)
        #pixmap = QPixmap(":/icons/windows1.jpg")
        MY_Btn = QPushButton('Close',self.helpwindow)
        MY_Btn.move(350,160)
        #MY_Btn.resize(30,30)
        MY_Btn.clicked.connect(self.helpwindow_close)
        #Lbl.setPixmap(pixmap)
        self.helpwindow.exec_() 
    
    @pyqtSlot(bool)
    def helpwindow_close(self, if_checkable_is_checked=None):
        self.helpwindow.hide()
        self.helpwindow.exec_() 
    
    @pyqtSlot(QCloseEvent)
    def closeEvent(self, event):
        '''
        This is the close event for the Window, not the tab. Need to 
        subclass the tab and assign closeEvent function to that subclass. 
        '''
        is_cancelled = False
        while self._tab.count()!=0 and not is_cancelled:
            self._tab.setCurrentIndex(self._tab.count()-1)
            choice = QMessageBox.question(self,'You have changes!',f'Save tab {self._tab.count()}?', \
                       QMessageBox.Save | QMessageBox.Discard|QMessageBox.Cancel)
            if choice == QMessageBox.Save:
                self.save_file()
            elif choice == QMessageBox.Discard:
                #Remove tab
                self._tab.remove_tab_current()
            else:
                event.ignore()
                is_cancelled = True
        
        if not is_cancelled: event.accept()

def main():
    app = QApplication(sys.argv)
    app.setStyleSheet('QMainWindow{background-color: #ffffff;border: 2px solid black;}')
    app.setWindowIcon(QIcon(':/icons/text_editor.png'))
    Gui = Window()
    app.exec_()
