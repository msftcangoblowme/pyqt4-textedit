'''This module is not accessible during app installation. Retrieve app version 
from package base folder, VERSION'''
with open('VERSION') as f:
    APP_VERSION = f.readline()

PROTOCOL_VERSION = '1.0'     # protocol version requested
APP_VERSION_RELEASE_NAME = "Lets hear you speak"